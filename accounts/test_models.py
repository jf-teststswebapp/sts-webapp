from django.test import TestCase
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone
from .models import *


# Locations Model Unit test
class LocationsModelTest(TestCase):
   @classmethod
   # Create a Location instance
   def setUpTestData(cls):
        cls.locations = Locations.objects.create(
            name="Locations"
        )

   # Test that the Location Name field exists in the created Location instance by running assertions on the specific properties that they are of a specific type, in this case str
   def test_it_has_information_fields(self):
        self.assertIsInstance(self.locations.name, str)

# Region Model Unit test
class RegionModelTest(TestCase):
   @classmethod
   # Create a Region instance
   def setUpTestData(cls):
        cls.region = Region.objects.create(
            Name="UK"
        )

   # Test that the Region Name field exists in the created Region instance by running assertions on the specific properties that they are of a specific type, in this case str
   def test_it_has_information_fields(self):
        self.assertIsInstance(self.region.Name, str)

# Emergency Contacts Model Unit test
class EmergencyContactsModelTest(TestCase):
   @classmethod
   # Create a Emergency Contacts instance
   def setUpTestData(cls):
        cls.EmergencyContacts = EmergencyContacts.objects.create(
            Oil_Spill_Responders="Maldives Port Authority"
        )

   # Test that the Oil Spill Responders field exists in the created Emergency Contacts instance by running assertions on the specific properties that they are of a specific type, in this case str
   def test_it_has_information_fields(self):
        self.assertIsInstance(self.EmergencyContacts.Oil_Spill_Responders, str)

# Equipment Details Model Unit test
class Equipment_DetailsModelTest(TestCase):
   @classmethod
   # Create a Equipment Details instance
   def setUpTestData(cls):
        cls.Equipment_Details = Equipment_Details.objects.create(
            Primary_Fenders="3.3m x 6.5m Yokohama pneumatic rubber fenders"
        )

   # Test that the Primary Fenders field exists in the created Equipment Details instance by running assertions on the specific properties that they are of a specific type, in this case str
   def test_it_has_information_fields(self):
        self.assertIsInstance(self.Equipment_Details.Primary_Fenders, str)



