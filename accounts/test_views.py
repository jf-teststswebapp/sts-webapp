from django.test import TestCase
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone
from .models import *
from django.contrib.auth import authenticate



# Unit Tests to check login functionality on the site with correct and incorrect combinations of username and password
class LoginTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='12test12', email='test@example.com')
        self.user.save()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_password(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_username_and_password(self):
        user = authenticate(username='wrong', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_no_username_and_wrong_password(self):
        user = authenticate(username='', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)
